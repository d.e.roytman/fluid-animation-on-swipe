//
//  ContentView.swift
//  Fluid swipe
//
//  Created by Dmitry Roytman on 21.02.2022.
//

import SwiftUI

final class ContentViewModel: ObservableObject {
  @Published var offset: CGSize = .zero
  @Published var isNextScreenPresented = false
  
  func onDragChangedAction(value: DragGesture.Value) {
    offset = value.translation
  }
  func onDragEndedAction() {
    if .halfScreenWidth < -offset.width {
      offset.width = -.screenWidth * 2
      isNextScreenPresented.toggle()
    } else {
      offset = .zero
    }
  }
  func onBackButtonAction() {
    isNextScreenPresented.toggle()
    offset = .zero
  }
}

struct ContentView: View {
  @ObservedObject var viewModel = ContentViewModel()
  
  var body: some View {
    ZStack {
      Color.blue
        .overlay(DescriptionView())
        .clipShape(SwipeShape(offset: viewModel.offset))
        .ignoresSafeArea()
        .overlay(
          PullView(viewModel: viewModel),
          alignment: .topTrailing
        )
        .padding(.trailing)
      if viewModel.isNextScreenPresented {
        NextScreenView {
          viewModel.onBackButtonAction()
        }
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}

// MARK: - Helper

extension CGFloat {
  fileprivate static var screenWidth: CGFloat {
    UIScreen.main.bounds.width
  }
  fileprivate static var halfScreenWidth: CGFloat { .screenWidth / 2 }
  fileprivate static var doubleScreenWidth: CGFloat { .screenWidth * 2 }
}
