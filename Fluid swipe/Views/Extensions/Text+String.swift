//
//  Text+String.swift
//  Fluid swipe
//
//  Created by Dmitry Roytman on 22.02.2022.
//

import SwiftUI

extension Text {
  init(string: String) {
    self.init(string)
  }
}
