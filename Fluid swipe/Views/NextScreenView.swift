//
//  NextScreenView.swift
//  Fluid swipe
//
//  Created by Dmitry Roytman on 22.02.2022.
//

import SwiftUI

struct NextScreenView: View {
  let onBackButtonAction: () -> Void
  var body: some View {
    VStack(spacing: .spacing) {
      Text(string: .title)
        .font(.largeTitle)
        .fontWeight(.heavy)
      Button(action: {
        withAnimation(.easeInOut) {
          onBackButtonAction()
        }
      }) {
        BackButtonText()
      }
    }
  }
}

struct NextScreenView_Previews: PreviewProvider {
  static var previews: some View {
    NextScreenView {}
  }
}

struct BackButtonText: View {
  var body: some View {
    Text(string: .backButtonText)
      .font(.callout)
      .padding(.horizontal , .horizontalPadding)
      .padding(.vertical , .verticalPadding)
      .foregroundColor(.white)
      .background(.red)
      .clipShape(Capsule())
  }
}

// MARK: - Helper

extension String {
  fileprivate static var backButtonText: String { "Back to the previous screen" }
  fileprivate static var title: String { "Sed ut perspiciatis unde omnis" }
}

extension CGFloat {
  fileprivate static var spacing: CGFloat { 20 }
  fileprivate static var horizontalPadding: CGFloat { 40 }
  fileprivate static var verticalPadding: CGFloat { 15 }
}
