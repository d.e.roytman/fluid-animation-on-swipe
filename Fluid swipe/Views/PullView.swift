//
//  PullView.swift
//  Fluid swipe
//
//  Created by Dmitry Roytman on 22.02.2022.
//

import SwiftUI

struct PullView: View {
  @ObservedObject var viewModel: ContentViewModel
  
  var body: some View {
    Image(systemName: "chevron.left")
      .font(.largeTitle)
      .frame(width: .imageWidth, height: .imageHeight)
      .contentShape(Rectangle())
      .foregroundColor(.blue)
      .gesture(
        DragGesture()
          .onChanged({ value in
            withAnimation(
              .interactiveSpring(
                response: .response,
                dampingFraction: .dampingFraction,
                blendDuration: .blendDuration
              )
            ) {
              viewModel.onDragChangedAction(value: value)
            }
          })
          .onEnded({ value in
            withAnimation(.spring()) {
              viewModel.onDragEndedAction()
            }
          })
      )
      .offset(x: .offsetX, y: .offsetY)
      .opacity(viewModel.offset == .zero ? .opaque : .translucent)
    
  }
}

struct PullView_Previews: PreviewProvider {
  static var previews: some View {
    PullView(
      viewModel: .init()
    )
  }
}

extension CGFloat {
  fileprivate static var imageWidth: CGFloat { 50 }
  fileprivate static var imageHeight: CGFloat { 50 }
  fileprivate static var offsetX: CGFloat { 10 }
  fileprivate static var offsetY: CGFloat { 58 }
}

extension Double {
  fileprivate static var opaque: CGFloat { 1 }
  fileprivate static var translucent: CGFloat { .zero }
  fileprivate static var response: CGFloat { 0.7 }
  fileprivate static var dampingFraction: CGFloat { 0.6 }
  fileprivate static var blendDuration: CGFloat { 0.6 }
}

