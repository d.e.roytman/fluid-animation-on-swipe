//
//  DescriptionView.swift
//  Fluid swipe
//
//  Created by Dmitry Roytman on 22.02.2022.
//

import SwiftUI

struct DescriptionView: View {
  var body: some View {
    VStack(alignment: .leading, spacing: .spacing) {
      Text(string: .title)
        .font(.largeTitle)
        .fontWeight(.heavy)
      Text(string: .caption)
        .font(.caption)
    }
      .padding(.horizontal)
      .foregroundColor(.white)
  }
}

struct DescriptionView_Previews: PreviewProvider {
  static var previews: some View {
    ZStack {
      DescriptionView()
    }
  }
}

// MARK: - Helper

extension String {
  fileprivate static var title: String { "Lorem Ipsum" }
  fileprivate static var caption: String {
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
  }
}

extension CGFloat {
  fileprivate static var spacing: CGFloat { 10 }
}
