//
//  SwipeShape.swift
//  Fluid swipe
//
//  Created by Dmitry Roytman on 22.02.2022.
//

import SwiftUI

struct SwipeShape: Shape {
  var offset: CGSize
  
  var animatableData: CGSize.AnimatableData {
    get { offset.animatableData }
    set { offset.animatableData = newValue }
  }
  
  func path(in rect: CGRect) -> Path {
    Path { path in
      // 1. Create rectangle shape
      path.move(to: .zero)
      path.addLine(to: .init(x: rect.maxX, y: .zero))
      path.addLine(to: .init(x: rect.maxX, y: rect.maxY))
      path.addLine(to: .init(x: .zero, y: rect.maxY))
      path.addLine(to: .zero)
      
      // 2. Create a dragging ledge
      path.move(to: CGPoint(x: rect.width, y: offset.fluidShapeYOffset))
      
      let fluidShapeControlPointYOffset = offset.fluidShapeControlPointYOffset
      
      // Control point
      let controlPointX = rect.makeOffset(with: offset) - .controlPointXOffset
      let controlPointY: CGFloat = .controlPointYOffset + fluidShapeControlPointYOffset / 2
      let controlPoint = CGPoint(x: controlPointX, y: controlPointY)
      path.addCurve(
        to: CGPoint(x: rect.width, y: fluidShapeControlPointYOffset),
        control1: controlPoint,
        control2: controlPoint
      )
    }
  }
}

// MARK: - Helper

extension CGFloat {
  fileprivate static var controlPointXOffset: CGFloat { 50 }
  fileprivate static var controlPointYOffset: CGFloat { 40 }
  fileprivate static var fluidShapeYOffset: CGFloat { 80 }
  fileprivate static var fluidShapeControlPointYOffset: CGFloat { 180 }
}

extension CGSize {
  fileprivate var fluidShapeYOffset: CGFloat {
    let offset: CGFloat = .fluidShapeYOffset + width
    return offset > .fluidShapeYOffset ? .fluidShapeYOffset : offset
  }
  fileprivate var fluidShapeControlPointYOffset: CGFloat {
    let offset = .fluidShapeControlPointYOffset + height - width
    return offset < .fluidShapeControlPointYOffset ? .fluidShapeControlPointYOffset : offset
  }
}

extension CGRect {
  fileprivate func makeOffset(with size: CGSize) -> CGFloat {
    width + (-size.width > .zero ? size.width : .zero)
  }
}
