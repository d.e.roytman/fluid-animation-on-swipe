//
//  Fluid_swipeApp.swift
//  Fluid swipe
//
//  Created by Dmitry Roytman on 21.02.2022.
//

import SwiftUI

@main
struct Fluid_swipeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
